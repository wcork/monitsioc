package cxro.epics;

import cxro.common.monit.MonitService;
import org.epics.pvdata.factory.FieldFactory;
import org.epics.pvdata.factory.PVDataFactory;
import org.epics.pvdata.factory.StandardFieldFactory;
import org.epics.pvdata.property.*;
import org.epics.pvdata.pv.*;
import org.epics.pvdatabase.PVRecord;

import java.util.Objects;

/**
 * MonitMonitorRecord
 *
 * @author wcork
 * created on 2019-04-03
 */

public class MonitMonitorRecord extends PVRecord
{
  private static final FieldCreate fieldCreate = FieldFactory.getFieldCreate();
  private static final PVDataCreate pvDataCreate = PVDataFactory.getPVDataCreate();
  private static final StandardField standardField = StandardFieldFactory.getStandardField();

  public static final Structure monitMonitorStructure = fieldCreate.createFieldBuilder()
      .add("timeStamp", standardField.timeStamp())
      .add("alarm", standardField.alarm())
      .add("name", standardField.scalar(ScalarType.pvString, "value"))
      .add("enabled", standardField.scalar(ScalarType.pvInt, "value"))
      .add("state", standardField.scalar(ScalarType.pvInt, "value"))
      .add("running", standardField.scalar(ScalarType.pvInt, "value,alarm"))
      .add("type", standardField.scalar(ScalarType.pvString, "value"))
      .createStructure();

  private final PVTimeStamp timeStamp = PVTimeStampFactory.create();
  private final PVString name;
  private final PVInt enabled;
  private final PVInt state;
  private final PVInt running;
  private final PVString type;
  private final PVAlarm runningAlarm = PVAlarmFactory.create();
  private final Alarm linkRunningAlarm = new Alarm();

  private MonitService service;
  private PVAlarm topAlarm = PVAlarmFactory.create();
  private Alarm linkTopAlarm = null;

  /**
   * Create a PVRecord that has pvStructure as it's top level structure.
   * A derived class must call super(recordName, pvStructure).
   *
   * @param recordName  The record name.
   * @param pvStructure The top level structure.
   */
  public MonitMonitorRecord(String recordName, PVStructure pvStructure, MonitService service) {
    super(recordName, pvStructure);
    this.service = service;
    this.timeStamp.attach(pvStructure.getSubField(PVStructure.class, "timeStamp"));
    this.name = pvStructure.getStringField("name.value");
    this.enabled = pvStructure.getIntField("enabled.value");
    this.state = pvStructure.getIntField("state.value");
    this.type = pvStructure.getStringField("type.value");
    this.running = pvStructure.getIntField("running.value");
    this.runningAlarm.attach(pvStructure.getSubField(PVStructure.class, "running.alarm"));
    this.topAlarm.attach(pvStructure.getSubField(PVStructure.class,"alarm"));
    this.process();
  }

  public static MonitMonitorRecord create(String recordName, PVStructure pvStructure, MonitService service) {
    return new MonitMonitorRecord(recordName, pvStructure, service);
  }

  public void updateService(MonitService updatedService, Alarm linkTopAlarm) {
    this.service = updatedService;
    this.linkTopAlarm = linkTopAlarm;
  }

  @Override
  public void process() {
    try {
      lock();
      beginGroupPut();

      if (this.linkTopAlarm == null) {
        TimeStamp timeStamp = TimeStampFactory.create();
        timeStamp.getCurrentTime();
        this.timeStamp.set(timeStamp);
        this.name.put(service.getName());
        this.enabled.put(service.isMonitored() ? 1 : 0);
        this.state.put(service.getStatus());
        this.running.put(service.isRunning() ? 1 : 0);
        this.type.put(service.getType().name());

        this.runningAlarm.get(this.linkRunningAlarm);
        if (!service.isMonitored() && !service.isRunning()) {
          this.linkRunningAlarm.setSeverity(AlarmSeverity.MINOR);
          this.linkRunningAlarm.setMessage(
              "Service is not monitored. " + service.getType().name() + " may still be running.");
          this.runningAlarm.set(this.linkRunningAlarm);
        } else {
          this.runningAlarm.set(new Alarm());
        }
        this.topAlarm.set(new Alarm());
      } else {
        this.topAlarm.set(this.linkTopAlarm);
      }

    } finally {
      endGroupPut();
      unlock();
    }
  }
}
