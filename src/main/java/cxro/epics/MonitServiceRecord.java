package cxro.epics;

import cxro.common.monit.MonitClient;
import cxro.common.monit.MonitService;
import org.epics.pvaccess.server.rpc.RPCRequestException;
import org.epics.pvaccess.server.rpc.RPCService;
import org.epics.pvdata.factory.FieldFactory;
import org.epics.pvdata.factory.PVDataFactory;
import org.epics.pvdata.factory.StandardFieldFactory;
import org.epics.pvdata.property.*;
import org.epics.pvdata.pv.*;
import org.epics.pvdatabase.PVDatabase;
import org.epics.pvdatabase.PVDatabaseFactory;
import org.epics.pvdatabase.PVRecord;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * MonitServiceRecord
 *
 * @author wcork
 * created on 2019-04-01
 */

public class MonitServiceRecord extends PVRecord
{
  private static final FieldCreate fieldCreate = FieldFactory.getFieldCreate();
  private static final PVDataCreate pvDataCreate = PVDataFactory.getPVDataCreate();
  private static final StandardField standardField = StandardFieldFactory.getStandardField();

  private static final String[] actionChoices = new String[]{"none", "enable", "disable", "start", "stop", "restart"};
  private static final Structure resultStructure = fieldCreate.createFieldBuilder().createStructure();

  private final Future fut_op;
  private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
  private final PVAlarm alarm = PVAlarmFactory.create();
  private final PVTimeStamp topTimeStamp = PVTimeStampFactory.create();
  private final MonitClient monitClient;
  private final PVString host;
  private final PVInt pollRate;
  private final PVStringArray serviceArray;
  private final Alarm linkAlarm = new Alarm();

  private boolean firstUpdate = true;
  private HashMap<String, MonitService> current_servicesMap;

  public MonitServiceRecord(String recordName, PVStructure pvStructure, MonitClient monitClient)
  {
    super(recordName, pvStructure);
    this.monitClient = monitClient;
    this.topTimeStamp.attach(pvStructure.getSubField(PVStructure.class, "timeStamp"));
    this.alarm.attach(pvStructure.getSubField(PVStructure.class, "alarm"));
    this.host = pvStructure.getSubField(PVString.class, "host.value");
    this.pollRate = pvStructure.getSubField(PVInt.class, "pollRate.value");
    this.serviceArray = pvStructure.getSubField(PVStringArray.class, "services.value");

    this.current_servicesMap = new HashMap<>(this.monitClient.getServices());

    this.process();

    // World's smallest runnable task declaration
    Runnable updateTask = this::process;

    this.fut_op = this.executor.scheduleAtFixedRate(updateTask, 0, 250, TimeUnit.MILLISECONDS);
  }

  public static MonitServiceRecord create(String recordName, String monitURL)
      throws IOException
  {
    return create(recordName, monitURL, "", "");
  }

  public static MonitServiceRecord create(String recordName, String monitURL, String monitUser, String monitPass)
      throws IOException
  {
    MonitClient monitClient;
    if (monitUser.equals("") | monitPass.equals("")) {
      monitClient = new MonitClient(monitURL);
    } else {
      monitClient = new MonitClient(monitURL, monitUser, monitPass);
    }

    Structure topStructure = fieldCreate.createFieldBuilder()
        .add("timeStamp", standardField.timeStamp())
        .add("alarm", standardField.alarm())
        .add("host", standardField.scalar(ScalarType.pvString, "value"))
        .add("pollRate", standardField.scalar(ScalarType.pvInt, "value,display"))
        .add("services", standardField.scalarArray(ScalarType.pvString, "value"))
        .createStructure();

    PVStructure pvMonitServiceRecordStructure = pvDataCreate.createPVStructure(topStructure);

    MonitServiceRecord monitServiceRecord = new MonitServiceRecord(recordName,
        pvMonitServiceRecordStructure, monitClient);

    PVDatabase master = PVDatabaseFactory.getMaster();
    master.addRecord(monitServiceRecord);
    return monitServiceRecord;
  }

  private String getServicePVName(String serviceName) {
    return this.getRecordName() + ":Service:" + serviceName;
  }

  private void action(String serviceName, String serviceAction)
      throws RPCRequestException
  {
    // Verify service is managed here
    if (!this.current_servicesMap.keySet().contains(serviceName)) {
      throw new RPCRequestException(Status.StatusType.ERROR, "Invalid serviceName");
    }
    // Verify action
    if (!Arrays.asList(MonitServiceRecord.actionChoices).contains(serviceAction)) {
      throw new RPCRequestException(Status.StatusType.ERROR, "Invalid serviceAction");
    }
    // Execute action
    try {
      MonitService targetService = this.current_servicesMap.get(serviceName);
      switch (serviceAction) {
        case "start":
          targetService.start();
          break;
        case "stop":
          targetService.stop();
          break;
        case "enable":
          targetService.monitor();
          break;
        case "disable":
          targetService.unmonitor();
          break;
        case "restart":
          targetService.restart();
          break;
      }
    } catch (IOException e) {
      throw new RPCRequestException(Status.StatusType.ERROR, e.getMessage());
    }
  }

  @Override
  public void process() {
    PVDatabase master = PVDatabaseFactory.getMaster();
    try {
      lock();
      this.monitClient.update(true);
      HashMap<String, MonitService> updated_servicesMap = this.monitClient.getServices();

      if (firstUpdate || !this.current_servicesMap.equals(updated_servicesMap)) {
        firstUpdate = false;
        TimeStamp timeStamp = TimeStampFactory.create();
        timeStamp.getCurrentTime();

        beginGroupPut();

        this.topTimeStamp.set(timeStamp);

        // Update the services list
        List<String> oldServiceList = new ArrayList<>(current_servicesMap.keySet());
        current_servicesMap = new HashMap<>(updated_servicesMap);
        List<String> updatedServiceList = new ArrayList<>(current_servicesMap.keySet());

        this.host.put(URI.create(this.monitClient.getHost()).getHost());
        this.pollRate.put(this.monitClient.getPollRateSeconds());
        PVString pollRateUnits = this.getPVStructure().getStringField("pollRate.display.units");
        pollRateUnits.put("S");

        this.serviceArray.put(0, updatedServiceList.size(), updatedServiceList.toArray(new String[0]), 0);
        this.serviceArray.setLength(updatedServiceList.size());

        // Remove records that went away
        oldServiceList.removeAll(updatedServiceList);
        oldServiceList.forEach(serviceName -> {
          PVRecord rec = master.findRecord(getServicePVName(serviceName));
          if (rec != null) {
            master.removeRecord(rec);
          }
        });

        updateServices(updatedServiceList, master, null);

        // Remove all alarms
        this.alarm.set(new Alarm());
        endGroupPut();
      }
    } catch (IOException ex) {
      this.alarm.get(this.linkAlarm);
      this.linkAlarm.setMessage("Update error");
      this.linkAlarm.setSeverity(AlarmSeverity.MAJOR);
      this.alarm.set(this.linkAlarm);
      updateServices(new ArrayList<>(this.current_servicesMap.keySet()), master, this.linkAlarm);
    } finally {
      unlock();
    }
  }

  private void updateServices(List<String> updatedServiceList, PVDatabase master, Alarm topAlarm) {
    for (String serviceName : updatedServiceList) {
      String serviceRecordName = getServicePVName(serviceName);
      MonitMonitorRecord findServiceRecord = (MonitMonitorRecord) master.findRecord(serviceRecordName);

      if (findServiceRecord != null) {
        findServiceRecord.updateService(current_servicesMap.get(serviceName), topAlarm);
        findServiceRecord.process();
      } else {
        PVStructure pvStruct = pvDataCreate.createPVStructure(MonitMonitorRecord.monitMonitorStructure);
        findServiceRecord = MonitMonitorRecord.create(serviceRecordName, pvStruct, current_servicesMap.get(serviceName));
        master.addRecord(findServiceRecord);
      }
    }
  }

  @Override
  public RPCService getService(PVStructure pvRequest)
  {
    PVString methodField = pvRequest.getSubField(PVString.class, "method");

    if (methodField != null) {
      String method = methodField.get();
      if (method.equals("action")) {
        return new ActionService(this);
      }
    }
    return null;
  }

  static class ActionService implements RPCService
  {
    private final MonitServiceRecord pvRecord;

    ActionService(MonitServiceRecord record) {
      pvRecord = record;
    }

    public PVStructure request(PVStructure args)
        throws RPCRequestException
    {
      // Handle args
      PVStructure queryField = args.getSubField(PVStructure.class, "query");
      if (queryField == null) {
        throw new RPCRequestException(Status.StatusType.ERROR, "No structure array value field");
      }
      Structure queryFieldStructure = queryField.getStructure();

      Scalar serviceField = queryFieldStructure.getField(Scalar.class, "service");
      if (serviceField == null || serviceField.getScalarType() != ScalarType.pvString) {
        throw new RPCRequestException(Status.StatusType.ERROR, "value field's structure has no string field service");
      }

      Scalar actionField = queryFieldStructure.getField(Scalar.class, "action");
      if (actionField == null || actionField.getScalarType() != ScalarType.pvString) {
        throw new RPCRequestException(Status.StatusType.ERROR, "value field's structure has no string field action");
      }

      // Do the action
      String service = queryField.getStringField("service").get();
      String action = queryField.getStringField("action").get();
      this.pvRecord.action(service, action);

      return pvDataCreate.createPVStructure(MonitServiceRecord.resultStructure);
    }
  }
}
