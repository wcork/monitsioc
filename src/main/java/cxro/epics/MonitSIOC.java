package cxro.epics;

import org.epics.pvaccess.PVAException;
import org.epics.pvaccess.client.ChannelProvider;
import org.epics.pvaccess.server.impl.remote.ServerContextImpl;
import org.epics.pvaccess.server.rpc.RPCServer;
import org.epics.pvaccess.server.rpc.RPCService;
import org.epics.pvdata.factory.FieldFactory;
import org.epics.pvdata.factory.PVDataFactory;
import org.epics.pvdata.pv.*;
import org.epics.pvdatabase.PVDatabase;
import org.epics.pvdatabase.PVDatabaseFactory;
import org.epics.pvdatabase.pva.ChannelProviderLocalFactory;
import org.ini4j.Ini;
import org.ini4j.IniPreferences;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;


public class MonitSIOC
{
  private static final PVDataCreate pvDataCreate = PVDataFactory.getPVDataCreate();
  private static final FieldCreate fieldCreate = FieldFactory.getFieldCreate();
  private static final String CONFIG_FILE_NAME = "MonitSIOC.ini";
  private static String RECORD_BASE_NAME;

  public static void main(String[] args)
      throws IOException, PVAException, BackingStoreException
  {
    // First we read from ini
    Path configPath = Paths.get("config", CONFIG_FILE_NAME);
    Preferences prefs = new IniPreferences(new Ini(configPath.toFile()));

    RECORD_BASE_NAME = prefs.node("Records").get("baseName", null);
    if (RECORD_BASE_NAME == null) {
      throw new IOException("Invalid config \"" + CONFIG_FILE_NAME + "\"");
    }
    String monitDaemonStr = prefs.node("MonitDaemons").get("urlList", null);
    if (monitDaemonStr == null) {
      throw new IOException("Invalid config \"" + CONFIG_FILE_NAME + "\"");
    }
    String[] monitDaemonArr = monitDaemonStr.split(",");

    // Start creating and adding records to PVAServer and RPCServer
    PVDatabase master = PVDatabaseFactory.getMaster();
    ChannelProvider channelProvider = ChannelProviderLocalFactory.getChannelProviderLocal();
    RPCServer monitRPCServer = new RPCServer();

    for (String daemonLocation : monitDaemonArr) {
      // Add PVRecord
      URI daemonUri = URI.create(daemonLocation);
      String recordName = RECORD_BASE_NAME + ":" + daemonUri.getHost();
      // Get user and pass from ini. Create record based on node existing
      MonitServiceRecord pvRecord;
      if (prefs.nodeExists(daemonUri.getHost())) {
        String user = prefs.node(daemonUri.getHost()).get("user", null);
        String pass = prefs.node(daemonUri.getHost()).get("pass", null);
        if (user == null || pass == null) {
          throw new IOException("Invalid config \"" + CONFIG_FILE_NAME + "\"");
        }
        pvRecord = MonitServiceRecord.create(recordName, daemonUri.toString(), user, pass);
      } else {
        pvRecord = MonitServiceRecord.create(recordName, daemonUri.toString());
      }
      pvRecord.setTraceLevel(2);
      master.addRecord(pvRecord);

      // Add RPCService
      Structure serviceRequestStructure = fieldCreate.createFieldBuilder()
          .add("method", ScalarType.pvString)
          .createStructure();
      PVStructure servicePVStructure = pvDataCreate.createPVStructure(serviceRequestStructure);
      PVString method = servicePVStructure.getStringField("method");
      method.put("action");
      RPCService monitRPCService = pvRecord.getService(servicePVStructure);
      monitRPCServer.registerService(recordName + ":RPC", monitRPCService);
    }

    // Start Services
    ServerContextImpl context = null;
    try {
      context = ServerContextImpl.startPVAServer(channelProvider.getProviderName(), 0, true, System.out);
      monitRPCServer.run(0);
    } finally {
      Objects.requireNonNull(context).destroy();
      monitRPCServer.destroy();
      master.destroy();
      channelProvider.destroy();
    }
  }
}
