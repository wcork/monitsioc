# MonitSIOC

An EPICS Soft IOC that is used to monitor and control services configured on remote Monit Daemons.

* Uses EPICS pvAccess for data as well as RPC.

## Configuration
Configuration is handled at runtime by reading from `config/MonitSIOC.ini`

Structure is as follows:
```ini
; This is where you define the global PV basename 
[Records]
baseName=BLXX:Monit

; Daemons to connect. urlList is of the format [URL],[URL]
[MonitDaemons]
urlList=http://localhost:2812,https://some-other-server:1234

; Required to give a host in [MonitDaemons]/urlList credentials.
; Skip this if no authentication is required to access the Monit daemon.
[localhost]
user=admin
pass=monit
```

## PV Structure
All daemons will have a structure created for summary reports as the following:

```bash
<baseName>:<URL> structure 
    time_t timeStamp  
        long secondsPastEpoch 
        int nanoseconds 
        int userTag 
    alarm_t alarm -------------------------------- // Used for connection/update errors 
        int severity
        int status 
        string message 
    epics:nt/NTScalar:1.0 host
        string value 
    epics:nt/NTScalar:1.0 pollRate
        int value 
        display_t display
            double limitLow 
            double limitHigh 
            string description 
            string format 
            string units S
    epics:nt/NTScalarArray:1.0 services ---------- // An array containing the services on the daemon
        string[] value []
```

All services for that daemon have the following structure:

```bash
<baseName>:<URL>:Service:<serviceName> structure 
    time_t timeStamp   
        long secondsPastEpoch 
        int nanoseconds 
        int userTag 
    alarm_t alarm -------------------------------- // Used for state errors
        int severity 
        int status 
        string message 
    epics:nt/NTScalar:1.0 name
        string value 
    epics:nt/NTScalar:1.0 enabled
        int value 
    epics:nt/NTScalar:1.0 state
        int value 
    epics:nt/NTScalar:1.0 running
        int value 
        alarm_t alarm 
            int severity 
            int status 
            string message 
    epics:nt/NTScalar:1.0 type
        string value
```

## RPC
RPC is implemented using the `pvcall` standard.

All RPC PV endpoints are of the following naming convention:

`<baseName>:<URL>:RPC`

With `service` and `action` as the arguments.

Actions can be executed on a service on a given daemon like to following:

`pvcall <baseName>:<URL>:RPC service=<serviceName> action=<action>`

Where `<serviceName>` are available services on the target URL and `<action>` is the action to take on the service.

### Available  actions
| action  | description |
| ------- | ---------------------------------------------------------------------------------------------------------------------- |
| enable  | Enable monitoring of the service                                                                                       |
| disable | Disable monitoring of the service. This will always set a low severity of the alarm associated with the service record |
| start   | Ask Monit to start the service if it has not been already                                                              |
| stop    | Ask Monit to stop the service                                                                                          |
| restart | Ask Monit to restart the service                                                                                       |

Example `pvcall`:

`pvcall BLXX:Monit:localhost:RPC service=sshd action=disable`

Will disable the monitoring of the sshd service on the local Monit daemon.

## TODO
* Add schema to RPC server.
